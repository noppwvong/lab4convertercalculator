package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Interest extends Activity implements OnClickListener{

	float interestRate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.calculate);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSet);
		b2.setOnClickListener(this);
		
		TextView intrTxt = (TextView)findViewById(R.id.intrTxt);
		interestRate = Float.parseFloat(intrTxt.getText().toString());
		
		TextView resultTxt = (TextView)findViewById(R.id.resultTxt);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
int id = v.getId();
		
		if (id == R.id.calculate) {
			EditText yearTxt = (EditText)findViewById(R.id.yearTxt);
			EditText moneyTxt = (EditText)findViewById(R.id.moneyTxt);
			TextView resultTxt = (TextView)findViewById(R.id.resultTxt);
			String res = "";
			try {
				float yr = Float.parseFloat(yearTxt.getText().toString());
				float money = Float.parseFloat(moneyTxt.getText().toString());
				float calculate = (float)(money*Math.pow(1+(interestRate/100), yr));
				res = String.format(Locale.getDefault(), "%.2f", calculate);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			resultTxt.setText(res);
		}
		else if (id == R.id.btnSet) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("interestRate", interestRate);
			startActivityForResult(i, 9998);
			//9999 is a request code. It is a user-defined unique integer.
			//It is used to identify the return value.
		}

}

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	if (requestCode == 9998 && resultCode == RESULT_OK) {
		interestRate = data.getFloatExtra("interestRate", 32.0f);
		TextView intrTxt = (TextView)findViewById(R.id.intrTxt);
		intrTxt.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
	}
}


}

